require_relative 'user_notes_query'
require_relative 'threads_query'

module Gitlab
  module Triage
    module GraphqlQueries
      class QueryBuilder
        def initialize(source_type, resource_type, conditions)
          @source_type = source_type.to_s.singularize
          @resource_type = resource_type
          @conditions = conditions
        end

        def resource_path
          [source_type, resource_type]
        end

        def query
          return if query_template.nil?

          format(query_template, source_type: source_type, resource_type: resource_type.to_s.camelize(:lower), group_query: group_query)
        end

        delegate :present?, to: :query_template

        private

        attr_reader :source_type, :resource_type, :conditions

        def query_template
          case conditions.dig(:discussions, :attribute).to_s
          when 'notes'
            UserNotesQuery
          when 'threads'
            ThreadsQuery
          end
        end

        def group_query
          return if source_type != 'group'

          ', includeSubgroups: true'
        end
      end
    end
  end
end
