# frozen_string_literal: true

require 'spec_helper'

describe 'generate summary' do
  include_context 'with integration context'

  let(:rule) do
    <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Rule name
              actions:
                summarize:
                  item: |
                    - [ ] [{{title}}]({{web_url}}) {{labels}}
                  title: |
                    Summary for {{type}}
                  summary: |
                    Start summary

                    {{items}}

                    End summary
    YAML
  end

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end
  end

  def stub_and_perform(rule, expected_title, expected_description)
    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      body: { title: expected_title, description: expected_description },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end

  it 'creates a summary issue' do
    expected_title = 'Summary for issues'
    expected_description = <<~MARKDOWN.chomp
    Start summary

    - [ ] [#{issue[:title]}](#{issue[:web_url]}) ~"#{issue.dig(:labels, 0)}"

    End summary
    MARKDOWN

    stub_and_perform(rule, expected_title, h(expected_description))
  end

  context 'when there is no resources' do
    let(:issues) { [] }

    it 'does not post the summary' do
      perform(rule)
    end
  end
end
