# frozen_string_literal: true

require 'spec_helper'

describe 'select merge requests by merged_at' do
  include_context 'with integration context'

  describe 'merged_at' do
    let!(:mr_from_3_months_ago) do
      mr.merge(merged_at: 3.months.ago)
    end
    let!(:mr_from_1_month_ago) do
      mr.merge(merged_at: 1.month.ago, iid: issue[:iid] * 2)
    end

    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [mr_from_3_months_ago, mr_from_1_month_ago]
      end
    end

    it 'comments on the merge request' do
      rule = <<~YAML
        resource_rules:
          merge_requests:
            rules:
              - name: Rule name
                conditions:
                  date:
                    attribute: merged_at
                    condition: newer_than
                    interval_type: months
                    interval: 2
                actions:
                  comment: |
                    Comment because its merged at date is newer than 2 months ago.
      YAML

      stub_post_mr_from_1_month_ago = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{mr_from_1_month_ago[:iid]}/notes",
        body: { body: 'Comment because its merged at date is newer than 2 months ago.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_mr_from_1_month_ago)
    end
  end
end
