# frozen_string_literal: true

require 'spec_helper'

describe 'select merge requests by target_branch' do
  include_context 'with integration context'

  describe 'target_branch' do
    let!(:mr_to_master) do
      mr.merge(target_branch: 'master')
    end
    let!(:mr_to_feature) do
      mr.merge(target_branch: 'feature', iid: issue[:iid] * 2)
    end

    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100, target_branch: 'master' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [mr_to_master]
      end
    end

    it 'comments on the merge request' do
      rule = <<~YAML
        resource_rules:
          merge_requests:
            rules:
              - name: Rule name
                conditions:
                  target_branch: 'master'
                actions:
                  comment: |
                    Comment because its target branch is `{{target_branch}}`.
      YAML

      stub_post_mr_to_master = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{mr_to_master[:iid]}/notes",
        body: { body: 'Comment because its target branch is `master`.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_mr_to_master)
    end
  end
end
