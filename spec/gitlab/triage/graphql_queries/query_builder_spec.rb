require 'spec_helper'

require 'gitlab/triage/graphql_queries/query_builder'

describe Gitlab::Triage::GraphqlQueries::QueryBuilder do
  let(:conditions) { {} }
  let(:query_builder) { described_class.new(source_type, resource_type, conditions) }

  describe '#resource_path' do
    subject { query_builder.resource_path }

    shared_examples 'resource path', :source_type, :resource_type do
      it 'prepares GraphQL query with proper source and resource types' do
        expect(subject).to eq([source_type.singularize, resource_type])
      end
    end

    context 'when source_type is projects' do
      let(:source_type) { 'projects' }
      let(:group_query) { nil }

      context 'when resource_type is issues' do
        let(:resource_type) { 'issues' }

        it_behaves_like 'resource path'
      end

      context 'when resource_type is merge_requests' do
        let(:resource_type) { 'merge_requests' }

        it_behaves_like 'resource path'
      end
    end

    context 'when source_type is groups' do
      let(:source_type) { 'groups' }
      let(:group_query) { ', includeSubgroups: true' }

      context 'when resource_type is issues' do
        let(:resource_type) { 'issues' }

        it_behaves_like 'resource path'
      end

      context 'when resource_type is merge_requests' do
        let(:resource_type) { 'merge_requests' }

        it_behaves_like 'resource path'
      end
    end
  end

  describe '#query' do
    subject(:graphql_query) { query_builder.query }

    shared_examples 'notes graphql query', :source_type, :resource_type do
      context 'when discussions->notes condition is provided' do
        let(:conditions) do
          {
            discussions: {
              attribute: :notes
            }
          }
        end

        let(:query) do
          <<-GRAPHQL
        query($source: ID!, $after: String, $iids: [String!]) {
          %{source_type}(fullPath: $source) {
            id
            %{resource_type}(after: $after, iids: $iids%{group_query}) {
              pageInfo {
                hasNextPage
                endCursor
              }
              nodes {
                id
                userNotesCount
              }
            }
          }
        }
          GRAPHQL
        end

        it 'prepares GraphQL query with proper source and resource types' do
          expect(graphql_query).to eq(
            format(query, source_type: source_type.singularize, resource_type: resource_type.camelize(:lower), group_query: group_query)
          )
        end
      end
    end

    shared_examples 'threads graphql query', :source_type, :resource_type do
      context 'when discussions->threads condition is provided' do
        let(:conditions) do
          {
            discussions: {
              attribute: :threads
            }
          }
        end

        let(:query) do
          <<-GRAPHQL
        query($source: ID!, $after: String, $iids: [String!]) {
          %{source_type}(fullPath: $source) {
            id
            %{resource_type}(after: $after, iids: $iids%{group_query}) {
              pageInfo {
                hasNextPage
                endCursor
              }
              nodes {
                id
                userDiscussionsCount
              }
            }
          }
        }
          GRAPHQL
        end

        it 'prepares GraphQL query with proper source and resource types' do
          expect(graphql_query).to eq(
            format(query, source_type: source_type.singularize, resource_type: resource_type.camelize(:lower), group_query: group_query)
          )
        end
      end
    end

    context 'when source_type is projects' do
      let(:source_type) { 'projects' }
      let(:group_query) { nil }

      context 'when resource_type is issues' do
        let(:resource_type) { 'issues' }

        it_behaves_like 'notes graphql query'
        it_behaves_like 'threads graphql query'
      end

      context 'when resource_type is merge_requests' do
        let(:resource_type) { 'merge_requests' }

        it_behaves_like 'notes graphql query'
        it_behaves_like 'threads graphql query'
      end
    end

    context 'when source_type is groups' do
      let(:source_type) { 'groups' }
      let(:group_query) { ', includeSubgroups: true' }

      context 'when resource_type is issues' do
        let(:resource_type) { 'issues' }

        it_behaves_like 'notes graphql query'
        it_behaves_like 'threads graphql query'
      end

      context 'when resource_type is merge_requests' do
        let(:resource_type) { 'merge_requests' }

        it_behaves_like 'notes graphql query'
        it_behaves_like 'threads graphql query'
      end
    end
  end
end
