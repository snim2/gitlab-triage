# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/base'
require 'gitlab/triage/resource/shared/issuable'

describe Gitlab::Triage::Resource::Shared::Issuable do
  include_context 'with network context'

  let(:resource) { {} }
  let(:name) { 'Gitlab::Triage::Resource::TestModel' }

  let(:model) do
    Class.new(Gitlab::Triage::Resource::Base) do
      include(Gitlab::Triage::Resource::Shared::Issuable)
    end
  end

  before do
    allow(model).to receive(:name).and_return(name)
  end

  subject do
    model.new(resource, network: network)
  end

  describe '#milestone' do
    context 'when there is no milestone' do
      it 'gives something falsey' do
        expect(subject.milestone).to be_falsey
      end
    end

    context 'when there is a milestone' do
      let(:resource) { { milestone: { id: 1 } } }

      it 'gives the milestone based on the resource' do
        milestone = subject.milestone

        expect(milestone).to be_kind_of(Gitlab::Triage::Resource::Milestone)
        expect(milestone.id).to eq(1)
      end
    end
  end

  describe '#labels' do
    let(:resource) { { labels: %w[this is an apple] } }

    it 'gives an array of Label with the respective names' do
      labels = subject.labels

      expect(labels.map(&:name)).to eq(resource[:labels])
    end

    it 'sets the parent to the resource' do
      labels = subject.labels

      expect(labels.map(&:parent)).to eq([subject] * 4)
    end
  end

  describe '#author' do
    let(:resource) { { author: { username: 'john_doe' } } }

    it 'returns the username of the author' do
      expect(subject.author).to eq('john_doe')
    end
  end

  context 'with label events' do
    # Issues can have labels which doesn't show in the events, because
    # events are only stored later.
    let(:resource) { { project_id: 452, iid: 321, labels: %w[old bug] } }

    let(:events_resource) do
      [
        {
          id: 1,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'add',
          label: { name: 'bug' }
        },
        {
          id: 2,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'add',
          label: { name: 'feature' }
        },
        {
          id: 3,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'remove',
          label: { name: 'bug' }
        },
        {
          id: 4,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'add',
          label: { name: 'bug' }
        },
        {
          id: 5,
          resource_type: 'Issue',
          resource_id: 135,
          action: 'remove',
          label: { name: 'feature' }
        }
      ]
    end

    before do
      stub_api(
        :get,
        "#{base_url}/projects/452/test_models/321/resource_label_events",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => network.options.token }) do
        events_resource
      end
    end

    describe '#label_events' do
      it 'gives an array of LabelEvent with the respective attributes' do
        events = subject.label_events

        expect(events).not_to be_empty

        events.each.with_index do |event, index|
          %i[id resource_type resource_id action].each do |field|
            expect(event.public_send(field))
              .to eq(events_resource.dig(index, field))
          end

          expect(event).to be_a(Gitlab::Triage::Resource::LabelEvent)
          expect(event.parent).to eq(subject)
          expect(event.label.parent).to eq(event)
          expect(event.label.name)
            .to eq(events_resource.dig(index, :label, :name))
        end
      end
    end

    describe '#labels_with_details' do
      it 'returns labels which have details from the events' do
        labels = subject.labels_with_details

        # It only has bug because feature is not in labels and
        # old is not in the events.
        expect(labels.map(&:name)).to eq(%w[bug])
        expect(labels.map(&:class)).to eq([Gitlab::Triage::Resource::Label])
      end

      context 'when one of the labels has since been deleted' do
        let(:events_resource) do
          [
            {
              id: 6,
              resource_type: 'Issue',
              resource_id: 135,
              action: 'add',
              label: nil
            }
          ]
        end

        it 'returns the other labels and ignores the deleted one' do
          labels = subject.labels_with_details

          expect(labels).to be_empty
        end
      end
    end

    describe '#labels_chronologically' do
      let(:events_resource) do
        [
          {
            id: 1,
            action: 'add',
            created_at: Time.new(2017, 1, 1),
            label: { name: 'bug' }
          },
          {
            id: 2,
            action: 'add',
            created_at: Time.new(2016, 1, 1),
            label: { name: 'old' }
          }
        ]
      end

      it 'returns the labels with detailed sorted chronologically' do
        labels = subject.labels_chronologically

        expect(labels.map(&:name)).to eq(%w[old bug])
      end
    end
  end

  describe '#root_id' do
    let(:resource) { { project_id: 111 } }
    let(:source_resource) { { id: 111, namespace: namespace_resource } }

    before do
      stub_api(
        :get,
        "#{base_url}/projects/111",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => network.options.token }) do
        source_resource
      end
    end

    context 'when the namespace does not have a parent' do
      let(:namespace_resource) { { id: 222 } }

      it 'returns the namespace id' do
        expect(subject.root_id).to eq(222)
      end
    end

    context 'when the namespace has a parent' do
      let(:namespace_resource) { { id: 222, parent_id: 333 } }
      let(:parent_resource) { { id: 333 } }

      before do
        stub_api(
          :get,
          "#{base_url}/groups/333",
          headers: { 'PRIVATE-TOKEN' => network.options.token }) do
          parent_resource
        end
      end

      it 'returns the parent id' do
        expect(subject.root_id).to eq(333)
      end
    end

    context 'when the namespace has a parent and which has a parent' do
      let(:namespace_resource) { { id: 222, parent_id: 333 } }
      let(:parent_resource) { { id: 333, namespace: { parent_id: 444 } } }
      let(:grandparent_resource) { { id: 444 } }

      before do
        stub_api(
          :get,
          "#{base_url}/groups/333",
          headers: { 'PRIVATE-TOKEN' => network.options.token }) do
          parent_resource
        end

        stub_api(
          :get,
          "#{base_url}/groups/444",
          headers: { 'PRIVATE-TOKEN' => network.options.token }) do
          grandparent_resource
        end
      end

      it 'returns the grandparent id' do
        expect(subject.root_id).to eq(444)
      end
    end

    context 'when the namespace has endless parents' do
      # When the parent is itself, it's a self reference which will not end
      let(:namespace_resource) { { id: 222, parent_id: 222 } }
      let(:parent_resource) { { id: 222, namespace: namespace_resource } }

      before do
        stub_api(
          :get,
          "#{base_url}/groups/222",
          headers: { 'PRIVATE-TOKEN' => network.options.token }) do
          parent_resource
        end
      end

      it 'raises described_class::SourceTooDeep' do
        expect do
          subject.root_id
        end.to raise_error(described_class::SourceTooDeep)
      end
    end
  end
end
