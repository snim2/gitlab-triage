require 'spec_helper'

require 'net/http'
require 'gitlab/triage/network_adapters/httparty_adapter'

describe Gitlab::Triage::NetworkAdapters::HttpartyAdapter do
  include_context 'with network context'

  subject { described_class.new(network_options) }

  let(:headers) do
    {
      "x-next-page": 2,
      "x-rate-limit-remaining": 999,
      "ratelimit-reset": 0
    }.with_indifferent_access
  end
  let(:success_response) do
    OpenStruct.new(
      headers: headers,
      parsed_response: "{ 'json': 'response' }"
    )
  end
  let(:url) { 'url' }

  describe '#get' do
    it 'raises HTTPFatalError on 500 error' do
      error_response = OpenStruct.new(
        response: Net::HTTPInternalServerError.new('HTTPV', 500, 'Internal Server Error')
      )
      allow(HTTParty).to receive(:get).and_return(error_response)

      expect do
        subject.get('token', url)
      end.to raise_error(Gitlab::Triage::Errors::Network::InternalServerError)
    end

    it 'raises TooManyRequests on 429 response' do
      error_response = OpenStruct.new(
        response: Net::HTTPTooManyRequests.new('HTTPV', 429, 'Too Many Requests')
      )
      allow(HTTParty).to receive(:get).and_return(error_response)

      expect do
        subject.get('token', url)
      end.to raise_error(Gitlab::Triage::Errors::Network::TooManyRequests)
    end

    context 'when processing more pages' do
      before do
        allow(HTTParty).to receive(:get).and_return(success_response)
      end

      it 'returns the correct url for more pages from first request' do
        results = subject.get('token', url)

        expect(results[:more_pages]).to eq(true)
        expect(results[:next_page_url]).to eq('url&page=2')
      end

      it 'returns the correct url if more pages are available' do
        url = 'url&page=10'

        results = subject.get('token', url)

        expect(results[:more_pages]).to eq(true)
        expect(results[:next_page_url]).to eq('url&page=2')
      end

      it 'returns the false flag if no more pages available' do
        headers['x-next-page'] = ""

        results = subject.get('token', url)

        expect(results[:more_pages]).to eq(false)
        expect(results[:next_page_url]).to eq(nil)
      end
    end
  end

  describe '#post' do
    it 'raises HTTPFatalError on 500 error' do
      error_response = OpenStruct.new(
        response: Net::HTTPInternalServerError.new('HTTPV', 500, 'Internal Server Error')
      )
      allow(HTTParty).to receive(:post).and_return(error_response)

      expect do
        subject.post('token', url, 'body')
      end.to raise_error(Gitlab::Triage::Errors::Network::InternalServerError)
    end

    it 'raises TooManyRequests on 429 response' do
      error_response = OpenStruct.new(
        response: Net::HTTPTooManyRequests.new('HTTPV', 429, 'Too Many Requests')
      )
      allow(HTTParty).to receive(:post).and_return(error_response)

      expect do
        subject.post('token', url, 'body')
      end.to raise_error(Gitlab::Triage::Errors::Network::TooManyRequests)
    end
  end
end
