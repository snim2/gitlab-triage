require 'spec_helper'

require 'gitlab/triage/command_builders/comment_command_builder'

describe Gitlab::Triage::CommandBuilders::CommentCommandBuilder do
  let(:lines) do
    [
      "comment",
      "/label ~bug",
      "/cc @username",
      "/close"
    ]
  end

  describe '#build_command' do
    it 'outputs the correct command' do
      builder = described_class.new(lines)
      expect(builder.build_command).to eq("comment\n\n/label ~bug\n\n/cc @username\n\n/close")
    end

    it 'outputs an empty string if no lines present' do
      builder = described_class.new([])
      expect(builder.build_command).to eq("")

      builder = described_class.new(nil)
      expect(builder.build_command).to eq("")
    end
  end
end
